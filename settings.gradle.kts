rootProject.name = "drv-xml"
include("drv-xml-model", "drv-xml-parser")
include("drv-json-model","drv-json-parser")
include("common-test")
